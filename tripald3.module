<?php

require_once('includes/stock_pedigree.inc');
require_once('includes/color_scheme.inc');

/**
 * Implements hook_libraries_info()
 */
function tripald3_libraries_info() {
  $libraries = array();
  $libraries['d3'] = array(
    'name' => 'D3.js',
    'vendor url' => 'http://d3js.org/',
    'download url' => 'https://github.com/mbostock/d3',
    'version arguments' => array(
      'file' => 'd3.js',
      'pattern' => '/\s*version: "(\d+\.\d+\.\d+)"/',
    ),
    'files' => array(
      'js' => array(
        'd3.min.js',
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_menu().
 */
function tripald3_menu() {
  $items = array();

  $items['ajax/tripal/d3-json/relationships/%/%'] = array(
    'title' => 'JS Graph: Tree',
    'page callback' => 'tripald3_get_relationship_json',
    'page arguments' => array(4,5),
    'access arguments' => array('view tripald3 json'),
    'type' => MENU_CALLBACK
  );

  $items['admin/tripal/extension/tripald3'] = array(
    'title' => 'Tripal D3 Diagrams',
    'description' => 'Adds d3.js diagrams to existing pages as well as providing an API for creating custom diagrams.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tripald3_common_settings_form'),
    'access arguments' => array('administer tripal'),
    'file' => 'includes/settings_forms.inc',
  );

  $items['admin/tripal/extension/tripald3/general'] = array(
    'title' => 'General',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tripald3_common_settings_form'),
    'access arguments' => array('administer tripal'),
    'file' => 'includes/settings_forms.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK
  );

  $items['admin/tripal/extension/tripald3/stock_pedigree'] = array(
    'title' => 'Stock Pedigree',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tripald3_stock_pedigree_settings_form'),
    'access arguments' => array('administer tripal'),
    'file' => 'includes/settings_forms.inc',
    'type' => MENU_LOCAL_TASK
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function tripald3_theme($existing, $type, $theme, $path) {
  $items = array();

  $items['tripald3_stock_pedigree'] = array(
    'template' => 'tripal_stock_pedigree',
    'path' => $path.'/templates',
    'variables' => array('node' => NULL),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function tripald3_permission() {
  return array(
    'view tripald3 json' => array(
      'title' => t('View Tripal D3 JSON'),
      'description' => t('Required for anyone wanting to view a Tripal D3 diagram.'),
    ),
    'view tripald3 pedigree' => array(
      'title' => t('View Tripal D3 Pedigree'),
      'description' => t('Required to view the pedigree pane on stock pages.'),
    ),
  );
}

/**
 * Implements hook_node_view(). Acts on all content types.
 */
function tripald3_node_view($node, $view_mode, $langcode) {

  switch ($node->type) {
    case 'chado_stock':
      if ($view_mode == 'full') {
        if(user_access('view tripald3 pedigree')) {

          // Only add the pedigree if there is more than the root node.
          if (!tripald3_is_node_leafless_tree($node->stock->stock_id)) {
            $node->content['tripal_stock_pedigree'] = array(
              '#theme' => 'tripald3_stock_pedigree',
              '#node' => $node,
              '#tripal_toc_id'    => 'pedigree',
              '#tripal_toc_title' => 'Pedigree'
            );
          }
          else {
            tripal_set_message(
              t('There is no pedigree for the current stock. If you feel there should
              be a pedigree, please check that you have <a href="@url">configured your stock
              relationship vocabulary correctly</a>.',
                array('@url' => url('admin/tripal/extension/tripald3/stock_pedigree'))),
              TRIPAL_WARNING
            );
          }
        }
      }
    break;
  }
}
